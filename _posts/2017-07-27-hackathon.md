---
layout: single
title:  "Intel Hackathon 2017!"
date:   2017-07-27 23:42:14 -0600
categories: news
sidebar: 
    nav: "side"
---

We have officially submitted our LATIS Project video for the 2017 Intel Hackathon.  Check it out here: 

{% include video id="Fl6h14X7Ggg" provider="youtube" %}

