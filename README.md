![Build Status](https://gitlab.com/rocket_launcher/jekyll/badges/master/build.svg)
![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

---

# Welcome
Welcome to the LATIS Project website repository. Have a look around and let us know what you think. We're always looking for useful feedback. The website itself can be found at [latisproject.org](http://latisproject.org)
