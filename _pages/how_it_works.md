---
layout: single
title: How It Works
permalink: /how_it_works/
sidebar: 
    nav: "side"
---

## Android

### Block Diagram
Coming soon

### Description of Methods
Coming soon

---
## Arduino

### Block Diagram

{% include figure image_path="/assets/images/main_site/Launch_Controller_Block_Diagram.svg" alt="Arduino 101 Launch Controller Software Block Diagram" caption="Arduino 101 Launch Controller Software Block Diagram" %}

### Launch State Definitions
- 0: Normal/aborted state
  - Normal/default state 
  - Turn off (digitalWrite(low)):
    - Igniter LED
    - Igniter (via relay)
    - Buzzer
    - Launch Warning LED
- 1: Launch warning state
  - Must have igniter interlock key to the armed position
  - Must not have pushed the abort button recently (30 seconds)
  - Turn on (digitalWrite(high)):
    - Launch Warning LED (flashing via CurieTimer PWM)
    - Buzzer
- 2: Launch state
  - Must have igniter interlock key to the armed position
  - Must not have pushed the abort button recently (30 seconds)
  - Turn on (digitalWrite(high)):
    - Igniter LED
    - Igniter (via relay)
  - Wait for rocket launch (8 seconds)
  - Return to normal state


### Description of Methods
#### launch_warning_start()
- Start the launch warning
- Called when user initiates the launch sequence (i.e. they push the launch button on the app)
- Turns on the following things:
  - Launch Warning LED (flashing via CurieTimer PWM)
  - Buzzer

#### launch_warning_stop()
- Stop the launch warning
- Called after a launch or after an abort
- Turns off the following things:
  - Igniter LED
  - Igniter (via relay)
  - Buzzer
  - Launch Warning LED

#### launch_rocket()
- Called after app countdown timer reaches zero
- Turns on the following things:
  - Igniter LED
  - Igniter (via relay)

#### check_safety_interlock_state()
- Called at begining of every iteration of the `BLE connected` loop and at the end of the main loop (when BLE is disconnected)
- Checks current state of the safety interlock key
  - Return value of ~4V (TRUE) indicates controller is READY for launch
    - Set `BLE safety characteristic` to `1`
    - Turn on Interlock Safety LED
  - Return value of ~0V (FALSE) indicates controller is NOT ready for launch
    - Set `BLE safety characteristic` to `0`
    - Turn off Interlock Safety LED
- return result (only used while a device is connected)

#### launch_abort()
- Called when launch abort button is pressed (via interrupt)
- Sets `launch_aborted` to TRUE
  - This flag is used to transition to the normal/aborted state at the next iteration of the loop (either `BLE connected` loop or main loop)
- Sets `launch_aborted_recently` to TRUE
  - This flag is used to prevent transition from the normal/aborted state for `launch_abort_cooldown_time` milliseconds

#### check_recent_aborts()
- Called near the end of very iteration of the `BLE connected` loop and at the end of the main loop (when BLE is disconnected)
- Checks if the `launch_aborted_recently` flag is set (TRUE)
  - If set, checks if the current time exceeds the cooldown time set by `launch_abort_cooldown_time`
  - If current time exceeds the cooldown time, reset (set to FALSE) the `launch_aborted_recently` flag 

#### on_ble_connect()
- Called every time a device connects to the arduino
- Turns on the link LED

#### on_ble_disconnect()
- Called every time a device disconnects from the arduino
- Stops the launch warning (just in case it was set) by calling `launch_warning_stop()`
- Sets the `BLE launch characteristic` to `0`
- Turns off the link LED

---
## iOS App
Coming Soon

---
## Flight Computer
Coming Soon

---
## Ground Station
Coming Soon

