---
layout: single
title: User Guide
permalink: /user_guide/
sidebar: 
    nav: "side"
---
{% include toc %}

## Android App Features
- Connect to a launch controller via bluetooth
- Remotely check the status of the controller
- Remotely launch a model rocket!!!

## Arduino 101 Controller Features
- Reliably launch rockets
- Provide launcher status wirelessly to the app
- Safety lockout & abort button

## iOS App Features
Coming soon

---
## Instructions for use
(Screenshots will be added soon)

1. Build a rocket
  - We tested the launcher with one of the [Estes kits](http://www.estesrockets.com/rockets/launch-sets/001469-tandem-xtm-amazontm-crossfire-isxtm) but this launcher will work with just about any rocket. 
1. Check the design & flight in [OpenRocket](http://openrocket.info/) to make sure you won't land your rocket on your neighbor's roof.
1. Pack the parachute & otherwise ready the rocket for launch
1. Go to an open area away from roofs, trees, powerlines, and other rocket catching obstacles
1. Set the launch pad on flat ground
1. Set the launch controller as far from the launch pad as the wires allow
  - The main concern is that the rocket motor exhaust will melt the enclosure and damage the controller. A few feet should do the trick but longer is better.
1. Set the rocket on the launch rod/pad
  - It's a good idea to double check the nose cone fit. Things tend to expand in the sun so make sure the fit is not too tight or too loose. Add or remove some masking tape around the base of the nosecone if necessary.
  - **DO NOT INSERT THE IGNITER INTO THE MOTOR** (yet)
1. Power on the launch controller
1. Make sure the safety interlock is not engaged (the safety interlock LED is off)
1. Connect the igniter leads to the controller clips
  - **Make sure the leads or clips are not touching as this is a short circuit (bad thing!)**
1. Insert the igniter into the motor
  - Verify the igniter leads and clips are still not touching
1. Double check for any other issues that can/would prevent a safe launch
  - Are kids/pets/other creatures far away from the rocket and launcher?
  - Are there any flammable things next to the launcher?
  - Anything else that's hazardous or unsafe?
1. Turn the safety interlock key to the engaged position (the safety interlock LED should now be on)
1. Launch the app
1. If this is the first time connecting to the launcher, add the launcher by pressing the (+) symbol in the upper right corner of the screen
1. Find the launcher in the list of nearby bluetooth devices. It will be called `RocketLauncher`. Add a name for the launcher. 
  - Bonus points for creative names.
1. Once the launcher is added, it will appear in the list of launchers on the main app screen.
1. Select the desired launcher (if you have more than one, make sure you know which one is which).
1. Make sure you are connected to the launcher
  - The blue link LED will turn on when a device connects to the launch controller
1. Move to a safe distance from the rocket (at least 15 feet, per the [safety code](http://www.nar.org/safety-information/model-rocket-safety-code/))
1. Verify the app shows the rocket is ready for launch
  - Safety interlock should be enabled (green in the app, yellow LED on controller on)
  - Comm link status should be green
1. Announce you will be launching the rocket
1. Verify the rocket is still safe to launch (no pets/kids/crazy uncles are near the rocket)
1. Press the arm button on the app
  - The background should change to a different color
1. Press *and hold* the launch button
  - The launch button must be held the entire time before the rocket is launched
  - This is a safety feature. The button acts as a [dead-man switch](https://en.wikipedia.org/wiki/Dead_man%27s_switch) so when something unusual or startling happens, the user simply has to let go of the button and the rocket is in a safe state
  - At this point there will be a loud buzzer and flashing light (launch warning LED) emanating from the launcher
1. Announce the count to the folks around you
  - It's both fun and important (safety)
1. When the count reaches zero the rocket should launch
  - The warning buzzer will continue to make sounds for another 8 seconds, just in case the rocket does not launch immediately
  - If the rocket does not launch, see the FAQ below
1. **Enjoy the rocket flight**
1. On your way to recovering the rocket *after it landed* stop by the launch controller and turn the safety interlock key to the disengaged position (safety interlock LED should be off)
1. Do it again!

---
## FAQ
### Rocket didn't launch
- **WAIT AT LEAST 60 SECONDS** before approaching the rocket, per the [safety code](http://www.nar.org/safety-information/model-rocket-safety-code/)
- Check the exposed wires for shorts (bare wires and clips should not be touching)
- Remove the igniter and see if it went off
  - Try a different igniter
- If the igniter did not go off, check the wire connections
- Try a different (new) battery for the igniter

### Can't connect to the controller
- Get closer to the controller (bluetooth range is about 40 ft)
- Change the arduino battery
- Restart the arduino (disconnect and reconnect the battery)
- Restart the app
- This might be caused by a [known bug](https://gitlab.com/rocket_launcher/android_app/issues/22). We'll hopefully fix it soon and if it affects you, please let us know. We'll work on it faster.

### Don't see launcher in list of devices
- Make sure bluetooth is turned 
- Get closer to the controller (bluetooth range is about 40 ft)
- Change the arduino battery
- Restart the arduino (disconnect and reconnect the battery)
- Restart the app

---
## Description of each button & LED
### Controller

| Item | Description |
| ---- | ----------- |
| Power LED | Turns on when power is applied to the Arduino (does not rely on functioning code | 
| Link LED | Turns on when the app connects to the controller. Turns off when it disconnects |
| Launch Warning LED | Blinks rapidly during the 10 second launch countdown |
| Rocket Launch LED | Turns on when power is applied to the igniter during launch |
| System Armed LED | This is the safety interlock LED. When on, launcher is armed and ready for launch. |
| Safety Key | This is the safety interlock. It arms the launcher for launch. |
| Launch Abort Button | Immediately aborts the launch and prevents further launches for 30 seconds |

<br>
### App
Coming soon

