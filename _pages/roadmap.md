---
layout: single
title: Roadmap / Build Plan
permalink: /roadmap/
sidebar: 
    nav: "side"
---

## Project Roadmap
{% include figure image_path="/assets/images/main_site/Build_Plan.svg" alt="LATIS Project Build Plan" caption="LATIS Project Build Plan" %}

---
### Spiral 1
Spiral 1 is the starting point for this project. The main focus is the launch controller and the phone app. At the end of this spiral, the user is able to launch a rocket using the launch controller and the app.

#### Android & iOS Roadmap
The app is able to receive launcher status and send a launch command to the launcher (controller).


#### Arduino 101 Controller Roadmap
The launch controller is able to check the state of the igniter, the safety interlock, and the batteries and send the state to the phone app. Once commanded, the launcher "turns on" the igniter which launches the rocket.

---
### Spiral 2
Spiral 2 adds a flight computer (one that actually flies on the rocket) that will take altitude (pressure) and GPS measurements and transmit them to a ground station. The flight computer will be able to control parachute ejection events and transmit its location to more easily find the rocket after its flight. Having the real-time location of the rocket is especially helpful for high altitude flights where the rocket can go beyond visual range.

#### Android & iOS Roadmap
The apps are able to receive live telemetry date from the rocket via the ground station.


#### Arduino 101 Controller Roadmap
Currently, no upgrades are planned for the launch controller in Spiral 2. 

#### Flight Computer Roadmap
The flight computer is developed entirely in this spiral. The flight computer takes altitude and position/velocity measurements, makes decisions on when to deploy parachutes, and transmits data to a groud station via a radio (non-bluetooth) link.

#### Ground Station Roadmap
The ground station is developed entirely in this spiral. The ground station receives data from the flight computer and sends it forward to the phone app via bluetooth.

---
### Spiral 3
Spiral 3 adds more measurements at the launch controller, mainly wind and launch rail angle. This spiral also adds [OpenRocket](http://openrocket.info/) integration. Having real-time wind measurements and launch rail angle will allow OpenRocket to provide more accurate estimates of the flight and landing area which increases safety.

#### Android & iOS Roadmap
The phone app is expanded to receive additional data from the launch controller and to communicate with a laptop running OpenRocket. Alternatively, the phone app is replaced entirely by an app running on the laptop that can more directly interface with OpenRocket.


#### Arduino 101 Controller Roadmap
Launch controller code is expanded to wirelessly connect (via BLE) to a wind anemometer and a launch rail angle & direction measurement device. This data is transmitted to the phone app.

#### Flight Computer Roadmap
Currently, no upgrades are planned for the flight computer in Spiral 3.

#### Ground Station Roadmap
Currently, no upgrades are planned for the ground station in Spiral 3. 


