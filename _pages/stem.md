---
layout: single
title: Education Resources
permalink: /stem/
sidebar: 
    nav: "side"
---

## Potential Lessons & Projects
A good place to start is the [Build Plan](/roadmap/). We can always use a hand at making the project better. Some other project ideas are below.

---
### Middle School

#### Build the controller based on step-by-step instructions
Coming soon

#### Soldering lessons
Coming soon

---
### High School

#### Build the controller based on schematics & wiring diagram
Coming soon

#### Design battery voltage checker circuit
Coming soon

---
### College

#### Implement a monte carlo simulation for accurate flight and landing zone estimates
Coming soon

---
### Graduate School
Coming soon

