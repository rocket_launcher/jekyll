---
layout: single
title: Contact Us
permalink: /contact_us/
sidebar: 
    nav: "side"
---

## Email
You can reach us at latisprojectorg at gmail dot com

## GitLab
We are also very involved with continuing development. GitLab provides a very nice set of communication tools via **issues**. You can visit the [main project page](https://gitlab.com/rocket_launcher) on GitLab or go to the individual issues pages for the [Arduino project](https://gitlab.com/rocket_launcher/controller/issues), the [Android project](https://gitlab.com/rocket_launcher/android_app/issues), or the [Jekyll project](https://gitlab.com/rocket_launcher/jekyll/issues). If you see a bug or would like a feature, please start a new issue (or let us know in the comments of an existing issue).
