---
layout: single
title: Overview
permalink: /overview/
sidebar: 
    nav: "side"
---

## Objective / Motivation
### The problem:
Amateur rocketry is a great educational opportunity for young students to extend their classroom instruction to a hands-on project to further their education in Science, Technology, Engineering, and Math (STEM). However, most amateur rocketry available to students focuses solely on the vehicle construction and flight aspects of rocketry, which misses opportunities to explore other aspects of STEM.

### The challenge:
Expand the educational opportunities of amateur rocketry to include other STEM fields.

### The solution:
Our project will allow a safe and exciting rocket launch using an Arduino 101 and a smart phone. This proposed launch controller, phone app, and flight computer introduces additional aspects of STEM into model rocketry - specifically computer science, electrical engineering, aerospace engineering, atmospheric science, and math. These additions will challenge and motivate interested students to pursue further educational opportunities and careers in STEM fields.

### The implementation:
The project is composed of three phases or Spirals. We know we will build Spiral 1. Spirals 2 and 3 will be attempted, given sufficient remaining time.

- Spiral 1 includes the basic launch controller powered by the Arduino 101 and phone app. The launch controller will connect to the rocket igniter and launch the rocket via the phone app. 

- Spiral 2 will add a second Arduino 101 as a flight computer that will fly inside of the rocket and will use the onboard sensors as an inertial measurement unit that relays in-flight telemetry data back to the ground. The flight computer will also integrate a GPS receiver for longer flights and easier rocket recovery. 

- Spiral 3 will add an accelerometer and magnetometer sensor to the launcher to precisely measure launch angle and direction. It will also add an anemometer to measure ground winds for safety. 

---
## Key Components & Features
- Phone App
  - Check status of launcher
  - Launch rocket
- Controller
  - Safely launch rocket from a distance
  - Provide launcher (controller) status to phone app
  - Check igniter status (future build)
  - Check battery state (future build)
