---
layout: single
title: About Us
permalink: /about/
sidebar: 
    nav: "side"
---

## Who we are
We're a group of ~~nerds~~ engineers from Rocket City, USA who like to play with code,
circuits, and rockets.

Here's what we do for the LATIS Project:
- Dmitriy Plaks    - Team Lead, software development (Arduino), webmaster
- Brooke Roberts   - Hardware development
- Travis Goodwyn   - Software development (iOS)
- Jonathan Townley - Software development (Android)


## How the idea came about
Brooke was eating pizza with his family at the best pizza joint in town and saw 
a little girl wearing a Rainbow Elementary Rockets tie-dye shirt.  This was 
just the spark that started the wheels turning towards a wireless rocket
launcher.  Dmitriy was the one who first heard about the Intel Hackathon and that
plus Brooke's initial idea was all the impetus we needed to get started.  When
Dmitriy told Jonathan and Travis about it, ideas for mobile app user interfaces 
started flowing and that was that.
