---
layout: single
title: Build/Download
permalink: /build_instructions/
sidebar: 
    nav: "side"
---
{% include toc %}


## Intro
So you want to launch a rocket with your phone? Well, THIS is the page for you, my friend. This page should provide you with enough instructions and resources so you can build your own rocket launcher. But we are human (not robot) so if something doesn't make sense, needs clarification, or is flat out wrong or missing please let us know but sending us an email or creating a new issue in [our repository](https://gitlab.com/rocket_launcher/jekyll).

---
## Parts List

| Part Description | Quantity | Notes |
| ---------------- | :------: | ----- |
| Arduino 101 | 1 | |
| Estes Rocket Kit | 1 | Many are available, [this](http://www.estesrockets.com/rockets/launch-sets/001469-tandem-xtm-amazontm-crossfire-isxtm) is the one we used |
| Buzzer | 1 | We used one from the [Grove Starter Kit](https://www.seeedstudio.com/Grove-Starter-Kit-for-Arduino-p-1855.html) |
| Green LED | 1 | | 
| Blue LED | 1 | | 
| Yellow LED | 2 | |
| Red LED | 1 | |
| Push Button | 1 | Just about any button will do |
| Key switch | 1 | Lots of options out there, we used [this](https://www.sparkfun.com/products/retired/10445) one|
| Resistor - 200 ohms | 1 | | 
| Resistor - 1M ohm | 3 | | 
| Relay, 5V | 1 | We used one from the [Grove Starter Kit](https://www.seeedstudio.com/Grove-Starter-Kit-for-Arduino-p-1855.html) |
| 9V battery | 2 | |
| 9V battery connector | 2 | |
| Breadboard | 1 | |
| Lots of wire | 20-30 ft | We used 20 AWG, stranded for the igniter connection | 
| Aligator clips | 2 | [These](https://www.sparkfun.com/products/111) work well |
| Enclosure | 1 | Preferrably one that's waterproof but just about any will work |

---
## Hardware Schematic

{% include figure image_path="/assets/images/main_site/Wiring_Diagram.svg" alt="Arduino 101 Launch Controller Wiring Diagram" caption="Arduino 101 Launch Controller Wiring Diagram" %}

### Arduino pin assignments

| Pin | Purpose | Wire color\* |
| --- | :-----: | :----------: |
| D0  | NC      | NC           |
| D1  | NC      | NC           |
| D2  | Igniter Relay | Light blue wire
| D3  | Link LED | Dark blue wire|
| D4 |  Launch Warning LED | Yellow wire|
| D5 |  Igniter LED | Red wire|
| D6 |  Safety Interlock LED | Yellow wire |
| D7 |  Buzzer | Purple wire |
| D8 |  Abort Button | Red wire |
| D9 | NC      | NC           |
| D10 | NC      | NC           |
| D11 | Safety Interlock (key switch) | Orange wire
| D12 | NC      | NC           |
| D13 | NC      | NC           |

NC: Not Connected

\* Wire color in the above diagram


---
## Software Description
If you're curious how all this code works, check out the [How it works](/how_it_works/) section.


---
## Downloads
You will need two separate things to get your launcher working: arduino 101 code and the android app (in the future, we hope to have an iPhone app as well). All of the project code, including this website is in our Gitlab [repository](https://gitlab.com/rocket_launcher/)

### Arduino 101 code

#### Pre-requisites:
To install this on your Arduino 101, make sure you have the following:
- [Arduino IDE](https://www.arduino.cc/en/Main/Software)
- Intel Curie library (do this via the Boards Manager menu in the Arduino IDE)

#### Actual installation:
- Download the latest stable build from the [master branch](https://gitlab.com/rocket_launcher/controller/raw/master/arduino_code/arduino_code.ino)
    - (unless you want to push the limits of our coding skills, in which case, help yourself to the [develop branch](https://gitlab.com/rocket_launcher/controller/raw/develop/arduino_code/arduino_code.ino) but we can't promise things won't break or otherwise won't work properly)
- Open the `arduino_code.ino` file in the Arduino IDE
- Connect your Arduino 101 and upload the code
- Make sure the hardware is wired up per the ["how to build it" guide](http://latisproject.org/build_instructions/)

### Android App
#### Download the app directly from the repository
- Latest stable version (download this one): [android app](https://gitlab.com/rocket_launcher/android_app/builds/artifacts/master/raw/app/build/outputs/apk/app-debug.apk?job=build)
- Current development/bleeding edge version (*don't download this one*, unless you're ready for things to break unexpectedly): [android app](https://gitlab.com/rocket_launcher/android_app/builds/artifacts/develop/raw/app/build/outputs/apk/app-debug.apk?job=build)

#### Download the app from Google Play Store
Coming soon

### iOS App
We're still working to develop this app. It's not quite ready for release (it is very broken at the moment) but if you want to give us a hand, let us know.


